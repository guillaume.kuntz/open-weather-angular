import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  data = null;
  city = 'Grenoble';
  countryCode = 'fr';

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.weatherService.getWeather(this.city, this.countryCode).subscribe(
        (apiData) => {
          this.data = apiData;
          console.log(this.data);
        }
    );
  }

}
